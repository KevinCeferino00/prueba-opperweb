<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'Categorias_id' => \App\Models\Categoria::inRandomOrder()->first()->id,
            'titulo' => $this->faker->text(150),
            'contenido' => $this->faker->text(rand(500,2000)),
            'fecha_creacion' => $this->faker->dateTimeBetween('-1 years','now'),
            'fecha_actualizacion' => $this->faker->dateTimeBetween('-1 years','now')
        ];
    }
}
