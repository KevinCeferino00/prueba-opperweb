<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CategoriaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->sentence(),
            'fecha_creacion' => $this->faker->dateTimeBetween('-1 years','now'),
            'fecha_actualizacion' => $this->faker->dateTimeBetween('-1 years','now')
        ];
    }
}
