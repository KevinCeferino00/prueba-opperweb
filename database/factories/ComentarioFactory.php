<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ComentarioFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'Posts_id' => \App\Models\Post::inRandomOrder()->first()->id,
            'contenido' => $this->faker->text(500),
            'fecha_creacion' => $this->faker->dateTimeBetween('-1 years','now'),
            'fecha_actualizacion' => $this->faker->dateTimeBetween('-1 years','now')
        ];
    }
}
