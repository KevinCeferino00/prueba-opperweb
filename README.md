
# Prueba API Opperweb

Api realizada con Laravel 8, para ingresar, a través de las url



## Instalación

Clona el proyecto

```bash
  git clone https://gitlab.com/KevinCeferino/prueba-opperweb.git
```

Ve al directorio

```bash
  cd prueba-opperweb
```

Instala las dependencias

```bash
  composer install
```
```bash
  npm install
```
Copia el archivo .env
```bash
composer run post-root-package-install
```
Genera tu llave
```bash
composer run post-create-project-cmd
```
Crea tu base de datos y agregala a un .env
```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=api
DB_USERNAME=root
DB_PASSWORD=
```
Corre las migraciones y los seeder
```bash
php artisan migrate:fresh --seed
```

Start the server

```bash
  php artisan serve
```


## Cómo acceder a las api

Para ver todos los registros a través del método ***GET***
```bash
http://127.0.0.1:8000/api/v1/categorias
//Si estás usando Laragon u otro servicion, reemplaza 127.0.0.1:8000 por tu url
```
Para ver los Posts  a través del método ***GET***
```bash
http://127.0.0.1:8000/api/v1/posts
//Si estás usando Laragon u otro servicion, reemplaza 127.0.0.1:8000 por tu url
```
Para ver los comentarios  a través del método ***GET***
```bash
http://127.0.0.1:8000/api/v1/comentarios
//Si estás usando Laragon u otro servicion, reemplaza 127.0.0.1:8000 por tu url
```
## Ver registros
### Ver Categoria
En la url ```http://127.0.0.1:8000/api/v1/categorias/1``` A través del método ***GET***
### Ver Posts
En la url ```http://127.0.0.1:8000/api/v1/posts/1``` A través del método ***GET***
### Ver Comentarios
En la url ```http://127.0.0.1:8000/api/v1/comentarios/1``` A través del método ***GET***
## Crear registros
### Crear categoria
En la url ```http://127.0.0.1:8000/api/v1/categorias``` A través del método ***POST***
```bash
{
    "nombre": "Categoria de prueba"
}
```
### Crear post
En la url ```http://127.0.0.1:8000/api/v1/posts``` A través del método ***POST***
```bash
{
    "Categorias_id" : 3,
    "titulo" : "Titulo de ejemplo",
    "contenido" : "Solo soy un contenido que puede extender un poquito jeje"
}
```
### Crear comentario
En la url ```http://127.0.0.1:8000/api/v1/comentarios``` A través del método ***POST***
```bash
{
    "Posts_id" : 5,
    "contenido" : "Solo soy un contenido que puede extender un poquito jeje"
}
```
## Actualizar registros
### Actualizar categoria
En la url ```http://127.0.0.1:8000/api/v1/categorias/1``` A través del método ***PUT***
```bash
{
    "nombre": "Categoria de prueba editada"
}
```
### Actualizar post
En la url ```http://127.0.0.1:8000/api/v1/posts/1``` A través del método ***PUT***
```bash
{
    "Categorias_id" : 6,
    "titulo" : "Titulo de ejemplo 2",
    "contenido" : "Solo soy un contenido que puede extender un poquito jeje 2"
}
```
### Actualizar comentario
En la url ```http://127.0.0.1:8000/api/v1/comentarios/8``` A través del método ***PUT***
```bash
{
    "contenido" : "Solo soy un contenido que puede extender un poquito jeje 2"
}
```
## Eliminar registros
### Eliminar Categoria
En la url ```http://127.0.0.1:8000/api/v1/categorias/1``` A través del método ***DELETE***
### Eliminar Posts
En la url ```http://127.0.0.1:8000/api/v1/posts/1``` A través del método ***DELETE***
### Eliminar Comentarios
En la url ```http://127.0.0.1:8000/api/v1/comentarios/1``` A través del método ***DELETE***
## Contacto

- Celular: [+573142665212](https://api.whatsapp.com/send?phone=573142665212)
- Correo Electrónico: [kevin.ceferino000@gmail.com](mailto:kevin.ceferino000@gmail.com)
