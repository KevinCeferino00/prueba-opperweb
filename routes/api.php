<?php

use App\Http\Controllers\Api\V1\CategoriaController;
use App\Http\Controllers\Api\V1\ComentarioController;
use App\Http\Controllers\Api\V1\PostController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//Para ver todas las rutas -> php artisan route:list


//Acceso a las rutas categorias -> /api/v1/categorias

Route::apiResource('v1/categorias',CategoriaController::class);


//Acceso a las rutas posts -> /api/v1/posts

Route::apiResource('v1/posts',PostController::class);


//Acceso a las rutas comentarios -> /api/v1/comentarios

Route::apiResource('v1/comentarios',ComentarioController::class);
