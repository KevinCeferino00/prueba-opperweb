<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\V1\Comentarios\ComentarioCollection;
use App\Http\Resources\V1\Comentarios\ComentarioResource;
use App\Models\Comentario;
use Illuminate\Http\Request;

class ComentarioController extends Controller
{

    //Se muestran los Comentarios, ya sea paginados o no -> /api/v1/comentarios -> Metodo: GET

    public function index()
    {

        //Devuelve los Comentarios paginados ->/api/v1/comentarios?page=2

        return new ComentarioCollection(Comentario::paginate());

        //Devuelve todos los Comentarios

        // return new ComentarioCollection(Comentario::all());
    }



    //Se agrega un registro solo si contiene Posts_id y contenido -> /api/v1/comentarios -> Metodo: POST

    public function store(Request $request)
    {
        if (\App\Models\Categoria::find($request->Posts_id)) {

            $request->validate([
                'Posts_id' => ['bail', 'required', 'integer'],
                'contenido' => ['bail', 'required', 'min:1', 'max:500']
            ]);

            $Comentario = Comentario::create($request->all());
            return response($Comentario, 201);
        }
        return response([
            'message' => 'El Comentario no existe o ID incorrecta'
        ], 400);
    }

    public function show(Comentario $Comentario)
    {
        return new ComentarioResource($Comentario);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'contenido' => ['min:1', 'max:500']
        ]);
        $Comentario = Comentario::findOrFail($id)->update($request->all());
        return response($Comentario, 200);
    }

    public function destroy($id)
    {
        if (Comentario::destroy($id)) {
            return response([
                'message' => 'El Comentario se ha eliminado con éxito'
            ]);
        }
        return response([
            'message' => 'Ha ocurrido un error, no se ha podido eliminar el Comentario'
        ]);
    }
}
