<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\V1\Categorias\CategoriaResource;
use App\Http\Resources\V1\Categorias\CategoriaCollection;
use App\Models\Categoria;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{

    public function index()
    {
        //Devuelve los categorias paginados ->/api/v1/categorias?page=2

        return new CategoriaCollection(Categoria::paginate());

        //Devuelve todos los Comentarios

        // return new CategoriaCollection(Categoria::all());
    }

    public function store(Request $request)
    {
        $request->validate([
            'nombre' => ['bail', 'required', 'min:1','max:150'],
        ]);

        $categoria = Categoria::create($request->all());
        if (!!$categoria) {
            return response($categoria, 201);
        }
        return response([
            'message' => "No se ha podido crear la categoría"
        ],400);
    }

    public function show(Categoria $Categoria)
    {
        return new CategoriaResource($Categoria);
    }

    public function update(Request $request, $id)
    {

        $request->validate([
            'nombre' => ['min:1','max:150']
        ]);
        $Categoria = Categoria::findOrFail($id)->update($request->all());
        return response($Categoria, 200);
    }

    public function destroy($id)
    {
        if (Categoria::destroy($id)) {
            return response([
                'message' => 'El Categoria se ha eliminado con éxito'
            ]);
        }
        return response([
            'message' => 'Ha ocurrido un error, no se ha podido eliminar el Categoria'
        ]);
    }
}
