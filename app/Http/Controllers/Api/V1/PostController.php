<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\V1\Posts\PostCollection;
use App\Http\Resources\V1\Posts\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{

    //Se muestran los posts, ya sea paginados o no -> /api/v1/posts -> Metodo: GET

    public function index()
    {

        //Devuelve los Posts paginados ->/api/v1/posts?page=2

        return new PostCollection(Post::paginate());

        //Devuelve todos los Posts

        // return new PostCollection(Post::all());
    }



    //Se agrega un registro solo si contiene Categorias_id, titulo y contenido -> /api/v1/posts -> Metodo: POST

    public function store(Request $request)
    {

        $request->validate([
            'Categorias_id' => ['bail', 'integer', 'required'],
            'titulo' => ['required','min:1' ,'max:150'],
            'contenido' => ['required', 'min:1' , 'max:65535']
        ]);


        //Se valida primero si el Id de la categoria existe, si no existe, se envia un mensaje avisando

        if (\App\Models\Categoria::find($request->Categorias_id)) 
        {
            $post = Post::create($request->all());
            return response($post, 201);
        }
        return response([
            'message' => 'La categoria no existe o ID incorrecta'
        ], 401);
    }



    //Se muestra el Post según si ID -> /api/v1/posts/{id} -> Metodo: GET

    public function show(Post $post)
    {
        return new PostResource($post);
    }



    //Se actualiza un registro según su ID -> /api/v1/posts/1 -> Metodo: PUT

    public function update(Request $request, $id)
    {
        $request->validate([
            'Categorias_id' => ['integer'],
            'titulo' => ['min:1','max:150'],
            'contenido' => ['min:1','max:65535']
        ]);
        if(\App\Models\Categoria::find($request->Categorias_id))
        {
            $post = Post::findOrFail($id)->update($request->all());
            if(!!$post)
        {
            return response($post, 200);
        }
        }
        return response([
            'message' => 'No se ha podido actualizar el Post'
        ],400);
        
    }



    //Se elimina un registro según su ID -> /api/v1/posts/1 -> Metodo: DELETE

    public function destroy($id)
    {
        if (Post::destroy($id)) 
        {
            return response([
                'message' => 'El Post se ha eliminado con éxito'
            ]);
        }
        return response([
            'message' => 'Ha ocurrido un error, no se ha podido eliminar el post o no existe'
        ]);
    }
}
