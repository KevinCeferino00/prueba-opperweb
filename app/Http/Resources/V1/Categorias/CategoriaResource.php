<?php

namespace App\Http\Resources\V1\Categorias;

use App\Http\Resources\V1\Posts\PostCollection;
use App\Models\Post;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoriaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'nombre' => $this->nombre,
            'posts' => new PostCollection(Post::where('Categorias_id', '=', $this->id)->paginate()), //entrega lo datos paginados
            // 'posts' => new PostCollection(Post::where('Categorias_id', '=', $this->id)->get()),   //Entrega Todos los datos
            'creada' => $this->created_at,
            'fecha_creacion' => $this->fecha_creacion,
            'fecha_actualizacion' => $this->fecha_actualizacion
        ];
    }
}
