<?php

namespace App\Http\Resources\V1\Posts;

use App\Http\Resources\V1\Comentarios\ComentarioCollection;
use App\Models\Comentario;
use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {


        //Formateamos nuestros valores a mostar

        return [
            'id'=> $this->id,
            'categoria_id' => $this->Categorias_id,
            'titulo' => $this->titulo,
            'contenido' => $this->contenido,
            'comentarios' => new ComentarioCollection(Comentario::where('Posts_id','=',$this->id)->paginate()), //Entrega los datos paginados
            // 'comentarios' => new ComentarioCollection(Comentario::where('Posts_id','=',$this->id)->get()), //Entrega todos los datos
            'publicado' => $this->published_at,
            'fecha_creacion' => $this->fecha_creacion,
            'fecha_actualizacion' => $this->fecha_actualizacion,
        ];
    }
}
