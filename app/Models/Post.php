<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;


    //Le indicamos a laravel que 'fecha_creacion' y 'fecha_actualizacion', son nuestros campos CREATED_AT y UPDATED_AT
    const CREATED_AT = 'fecha_creacion';
    const UPDATED_AT = 'fecha_actualizacion';


    protected $fillable = [
        'Categorias_id',
        'titulo',
        'contenido',
    ];


    //Hacemos referncia de nuestra Llave Foreana para acceder a los valores de Categorias

    public function categoria()
    {
        return $this->belongsTo(Categoria::class, 'Categorias_id');
    }


    //Hacemos referncia de nuestra referencia para traer información desde Comentarios, y cerramos la relacion 1 a muchos

    public function comentarios()
    {
        return $this->hasMany(Comentario::class, 'Posts_id');
    }


    //getter para obtener el diffForHumans del campo 'fecha_creacion' (2 Months ago) {{ $this->publised_at }}
    public function getPublishedAtAttribute()
    {
        return $this->fecha_creacion->diffForHumans();
    }
}
