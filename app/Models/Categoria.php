<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    use HasFactory;


    //Le indicamos a laravel que 'fecha_creacion' y 'fecha_actualizacion', son nuestros campos CREATED_AT y UPDATED_AT
    const CREATED_AT = 'fecha_creacion';
    const UPDATED_AT = 'fecha_actualizacion';


    protected $fillable = [
        'nombre'
    ];


    //Hacemos referncia de nuestra referencia para traer información desde Posts, y cerramos la relacion 1 a muchos
    public function posts()
    {
        return $this->hasMany(Post::class, 'Categorias_id');
    }


    //getter para obtener el diffForHumans del campo 'fecha_creacion' (2 Months ago) {{ $this->publised_at }}
    public function getCreatedAtAttribute()
    {
        return $this->fecha_creacion->diffForHumans();
    }
}
