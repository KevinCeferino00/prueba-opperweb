<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    use HasFactory;


    //Le indicamos a laravel que 'fecha_creacion' y 'fecha_actualizacion', son nuestros campos CREATED_AT y UPDATED_AT
    const CREATED_AT = 'fecha_creacion';
    const UPDATED_AT = 'fecha_actualizacion';

    protected $fillable=[
        'Posts_id',
        'contenido'
    ];


    //Hacemos referncia de nuestra Llave Foreana para acceder a los valores de Posts
    public function post()
    {
        return $this->belongsTo(Post::class, 'Posts_id');
    }


    //getter para obtener el diffForHumans del campo 'fecha_creacion' (2 Months ago) {{ $this->publised_at }}
    public function getPublishedAtAttribute()
    {
        return $this->fecha_creacion->diffForHumans();
    }
}
